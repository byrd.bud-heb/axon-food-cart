package com.example.axonexample.command;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodCartCommandApplication {
    public static void main(String[] args) {
        SpringApplication.run(FoodCartCommandApplication.class, args);
    }
}
