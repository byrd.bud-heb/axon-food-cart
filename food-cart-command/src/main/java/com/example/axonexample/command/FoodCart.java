package com.example.axonexample.command;

import com.example.axonexample.core.command.ConfirmOrderCommand;
import com.example.axonexample.core.command.CreateFoodCartCommand;
import com.example.axonexample.core.command.DeselectProductCommand;
import com.example.axonexample.core.command.SelectProductCommand;
import com.example.axonexample.core.event.FoodCartCreatedEvent;
import com.example.axonexample.core.event.OrderConfirmedEvent;
import com.example.axonexample.core.event.ProductDeselectedEvent;
import com.example.axonexample.core.event.ProductSelectedEvent;
import com.example.axonexample.core.exception.ConfirmedFoodCartModificationException;
import com.example.axonexample.core.exception.DuplicateFoodCartConfirmationException;
import com.example.axonexample.core.exception.ProductNotInFoodCartException;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

/**
 * Food cart aggregate.
 */
@Data
@Aggregate
@NoArgsConstructor
public class FoodCart {
    /**
     * Food cart aggregate ID.
     */
    @AggregateIdentifier
    private UUID foodCartId;

    /**
     * Products added to the cart, along with their quantities.
     */
    private Map<UUID, Integer> selectedProducts;

    /**
     * Whether the order is confirmed.
     */
    private boolean confirmed;

    /**
     * Handle {@link CreateFoodCartCommand} commands.
     *
     * @param command Command to process.
     */
    @CommandHandler
    public FoodCart(CreateFoodCartCommand command) {
        apply(new FoodCartCreatedEvent(UUID.randomUUID()));
    }

    /**
     * Handle {@link SelectProductCommand} commands.
     * <p>
     * A confirmed food cart may not be modified, and an attempt to add products to a confirmed food cart will
     * result in a {@link ConfirmedFoodCartModificationException} to be thrown.
     *
     * @param command Command to process.
     */
    @CommandHandler
    public void handle(SelectProductCommand command) {
        if (confirmed) {
            throw new ConfirmedFoodCartModificationException(command.foodCartId());
        }

        apply(new ProductSelectedEvent(command.foodCartId(), command.productId(), command.quantity()));
    }

    /**
     * Handle {@link DeselectProductCommand} commands.
     * <p>
     * A confirmed food cart may not be modified, and an attempt to remove products from a confirmed food cart will
     * result in a {@link ConfirmedFoodCartModificationException} to be thrown.
     * <p>
     * A product must have been added to the food card to remove it. Otherwise, a
     * {@link ProductNotInFoodCartException} is thrown.
     *
     * @param command Command to process.
     */
    @CommandHandler
    public void handle(DeselectProductCommand command) {
        if (confirmed) {
            throw new ConfirmedFoodCartModificationException(command.foodCartId());
        } else if (!selectedProducts.containsKey(command.productId())) {
            throw new ProductNotInFoodCartException(command.foodCartId(), command.productId());
        }

        apply(new ProductDeselectedEvent(command.foodCartId(), command.productId(), command.quantity()));
    }

    /**
     * Handle {@link ConfirmOrderCommand} commands.
     * <p>
     * A food cart may not be confirmed more than once, and a {@link DuplicateFoodCartConfirmationException} will be thrown
     * if subsequent attempts are made.
     *
     * @param command Command to process.
     */
    @CommandHandler
    public void handle(ConfirmOrderCommand command) {
        if (confirmed) {
            throw new DuplicateFoodCartConfirmationException(command.foodCartId());
        }

        apply(new OrderConfirmedEvent(command.foodCartId()));
    }

    /**
     * Process {@link FoodCartCreatedEvent} events.
     * <p>
     * Primarily instantiates the aggregate with its default values.
     *
     * @param event Event to process.
     */
    @EventSourcingHandler
    public void on(FoodCartCreatedEvent event) {
        foodCartId = event.foodCartId();
        selectedProducts = new HashMap<>();
        confirmed = false;
    }

    /**
     * Process {@link ProductSelectedEvent} events.
     * <p>
     * A product (and its quantity) is added to the aggregate. If the product was already present, its quantity is
     * updated.
     *
     * @param event Event to process.
     */
    @EventSourcingHandler
    public void on(ProductSelectedEvent event) {
        selectedProducts.merge(event.productId(), event.quantity(), Integer::sum);
    }

    /**
     * Process {@link ProductDeselectedEvent} events.
     * <p>
     * A product (and its quantity) is removed from the aggregate. If the resulting quantity is less than or equal to
     * zero, it will be removed from the food cart entirely.
     *
     * @param event Event to process.
     */
    @EventSourcingHandler
    public void on(ProductDeselectedEvent event) {
        int result = selectedProducts.merge(event.productId(), event.quantity(), (p, n) -> p - n);

        if (result <= 0) {
            selectedProducts.remove(event.productId());
        }
    }

    /**
     * Process {@link OrderConfirmedEvent} events.
     * <p>
     * The food cart's {@code confirmed} flag will be set to {@code true}.
     *
     * @param event Event to process.
     */
    @EventSourcingHandler
    public void on(OrderConfirmedEvent event) {
        confirmed = true;
    }
}
