package com.example.axonexample.command

import com.example.axonexample.core.command.ConfirmOrderCommand
import com.example.axonexample.core.command.CreateFoodCartCommand
import com.example.axonexample.core.command.DeselectProductCommand
import com.example.axonexample.core.command.SelectProductCommand
import com.example.axonexample.core.event.FoodCartCreatedEvent
import com.example.axonexample.core.event.OrderConfirmedEvent
import com.example.axonexample.core.event.ProductDeselectedEvent
import com.example.axonexample.core.event.ProductSelectedEvent
import com.example.axonexample.core.exception.ConfirmedFoodCartModificationException
import com.example.axonexample.core.exception.DuplicateFoodCartConfirmationException
import com.example.axonexample.core.exception.ProductNotInFoodCartException
import org.axonframework.test.aggregate.AggregateTestFixture
import org.axonframework.test.aggregate.FixtureConfiguration
import spock.lang.Specification

import static com.example.axonexample.command.util.EventMatcher.matchOne

class FoodCartSpec extends Specification {
    FixtureConfiguration<FoodCart> fixture

    def setup() {
        fixture = new AggregateTestFixture<>(FoodCart)
    }

    def 'When a new food cart is requested, the command is successful and a FoodCartCreatedEvent is emitted'() {
        expect:
        fixture.givenNoPriorActivity()
            .when(new CreateFoodCartCommand())
            .expectSuccessfulHandlerExecution()
            .expectEventsMatching(matchOne {
                it instanceof FoodCartCreatedEvent
            })
    }

    def 'When a food cart has a new product added to it, a ProductSelectedEvent is emitted with the requested quantity'() {
        setup:
        def foodCartId = UUID.randomUUID()
        def productId = UUID.randomUUID()

        expect:
        fixture.given(new FoodCartCreatedEvent(foodCartId))
            .when(new SelectProductCommand(foodCartId, productId, 5))
            .expectSuccessfulHandlerExecution()
            .expectEvents(new ProductSelectedEvent(foodCartId, productId, 5))
    }

    def 'When a food cart has a product added that already existed in the cart, a ProductSelectedEvent is emitted with the requested quantity'() {
        setup:
        def foodCartId = UUID.randomUUID()
        def productId = UUID.randomUUID()

        expect:
        fixture.given(new FoodCartCreatedEvent(foodCartId))
            .andGiven(new ProductSelectedEvent(foodCartId, productId, 5))
            .when(new SelectProductCommand(foodCartId, productId, 10))
            .expectSuccessfulHandlerExecution()
            .expectEvents(new ProductSelectedEvent(foodCartId, productId, 10))
            .expectState { it.selectedProducts[productId] == 15 }
    }

    def 'When a confirmed food cart has a product added, a ConfirmedFoodCartModificationException is thrown'() {
        setup:
        def foodCartId = UUID.randomUUID()
        def productId = UUID.randomUUID()

        expect:
        fixture.given(new FoodCartCreatedEvent(foodCartId))
            .andGiven(new OrderConfirmedEvent(foodCartId))
            .when(new SelectProductCommand(foodCartId, productId, 10))
            .expectException(ConfirmedFoodCartModificationException)
    }

    def 'When a food cart has a product removed that was previously added to the cart with a resulting quantity that is > 0, a ProductDeselectedEvent is emitted with the requested quantity and the quantity is updated'() {
        setup:
        def foodCartId = UUID.randomUUID()
        def productId = UUID.randomUUID()

        expect:
        fixture.given(new FoodCartCreatedEvent(foodCartId))
            .andGiven(new ProductSelectedEvent(foodCartId, productId, 10))
            .when(new DeselectProductCommand(foodCartId, productId, 5))
            .expectSuccessfulHandlerExecution()
            .expectEvents(new ProductDeselectedEvent(foodCartId, productId, 5))
            .expectState { it.selectedProducts[productId] == 5 }
    }

    def 'When a food cart has a product removed that was previously added to the cart with a resulting quantity that is == 0, a ProductDeselectedEvent is emitted with the requested quantity and the product is removed from the cart'() {
        setup:
        def foodCartId = UUID.randomUUID()
        def productId = UUID.randomUUID()

        expect:
        fixture.given(new FoodCartCreatedEvent(foodCartId))
            .andGiven(new ProductSelectedEvent(foodCartId, productId, 5))
            .when(new DeselectProductCommand(foodCartId, productId, 5))
            .expectSuccessfulHandlerExecution()
            .expectEvents(new ProductDeselectedEvent(foodCartId, productId, 5))
            .expectState { !it.selectedProducts.containsKey(productId) }
    }

    def 'When a food cart has a product removed that was previously added to the cart with a resulting quantity that is < 0, a ProductDeselectedEvent is emitted with the requested quantity and the product is removed from the cart'() {
        setup:
        def foodCartId = UUID.randomUUID()
        def productId = UUID.randomUUID()

        expect:
        fixture.given(new FoodCartCreatedEvent(foodCartId))
            .andGiven(new ProductSelectedEvent(foodCartId, productId, 5))
            .when(new DeselectProductCommand(foodCartId, productId, 10))
            .expectSuccessfulHandlerExecution()
            .expectEvents(new ProductDeselectedEvent(foodCartId, productId, 10))
            .expectState { !it.selectedProducts.containsKey(productId) }
    }

    def 'When a food cart has a product removed that was not previously added to the cart, a ProductNotInFoodCartException is thrown'() {
        setup:
        def foodCartId = UUID.randomUUID()
        def productId = UUID.randomUUID()

        expect:
        fixture.given(new FoodCartCreatedEvent(foodCartId))
            .when(new DeselectProductCommand(foodCartId, productId, 10))
            .expectException(ProductNotInFoodCartException)
    }

    def 'When a food cart is confirmed for the first time, an OrderConfirmedEvent is emitted and the confirmed flag is set to true'() {
        setup:
        def foodCartId = UUID.randomUUID()

        expect:
        fixture.given(new FoodCartCreatedEvent(foodCartId))
            .when(new ConfirmOrderCommand(foodCartId))
            .expectSuccessfulHandlerExecution()
            .expectEvents(new OrderConfirmedEvent(foodCartId))
            .expectState { it.confirmed }
    }

    def 'When a food cart is confirmed after already having been confirmed, a DuplicateFoodCartConfirmationException is thrown'() {
        setup:
        def foodCartId = UUID.randomUUID()

        expect:
        fixture.given(new FoodCartCreatedEvent(foodCartId))
            .andGiven(new OrderConfirmedEvent(foodCartId))
            .when(new ConfirmOrderCommand(foodCartId))
            .expectException(DuplicateFoodCartConfirmationException)
    }
}
