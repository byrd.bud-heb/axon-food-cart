package com.example.axonexample.command.util;


import org.axonframework.messaging.Message;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

import java.util.List;
import java.util.function.Function;

@SuppressWarnings("unchecked")
public class EventMatcher {
    public static <T> Matcher<List<T>> matchOne(Function<T, Boolean> function) {
        return new BaseMatcher<>() {
            @Override
            public boolean matches(Object actual) {
                if (!(actual instanceof List)) {
                    return false;
                } else if (((List<Message<T>>) actual).size() != 1) {
                    return false;
                } else {
                    return function.apply(((List<Message<T>>) actual).get(0).getPayload());
                }
            }

            @Override
            public void describeTo(Description description) {

            }
        };
    }

    public static <T> Matcher<List<T>> matchAll(Function<List<T>, Boolean> function) {
        return new BaseMatcher<>() {
            @Override
            public boolean matches(Object actual) {
                if (!(actual instanceof List)) {
                    return false;
                } else {
                    return function.apply(((List<Message<T>>) actual).stream().map(Message::getPayload).toList());
                }
            }

            @Override
            public void describeTo(Description description) {

            }
        };
    }
}
