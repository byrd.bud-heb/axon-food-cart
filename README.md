# Axon Food Cart Example Project

I wrote this application using Axon Framework and Axon Server to learn that ecosystem and the CQRS architectural
pattern. I followed the [Food Cart sample project](https://github.com/AxonIQ/food-ordering-demo) video series, however,
I took some liberties around project structure to fit what I believe to be a better Spring application layout.
I also broke the application into microservices and a base library project to emulate what a larger and more mature
project might look like.

## Starting Axon Server

A Docker compose file exists at the root of the project and may be used to launch an Axon Server container instance.
It is configured to use the host and name of `localhost` and its default ports (`8024` and `8124`) are exposed.

To star the container, run the following command:

```shell
docker compose up -d
```

This will start the container in the background. If you prefer to view the server's logs, simply omit the `-d` argument.

You may access the administrative UI of Axon Server at `http://localhost:8024`.

## Starting the Command Processor Application

To start the command processing application (which contains the FoodCart aggregate), run the application under the
`food-cart-command` directory. You can do this within an IDE or using Gradle as follows:

```shell
./gradlew :food-cart-command:bootRun
```

The application is configured to talk to the Axon Server container and should require no additional configuration.

## Starting the Query Processor Application

To start the query processing application, run the application under the `food-cart-query` directory. You can do this
within an IDE or using Gradle as follows:

```shell
./gradlew :food-cart-query:bootRun
```

The application is configured to talk to the Axon Server container and should require no additional configuration.
The application exposes a web API on port 8080. See the
[`FoodOrderingController`](food-cart-query/src/main/java/com/example/axonexample/query/controller/FoodOrderingController.java)
class to discover what API endpoints exist.
