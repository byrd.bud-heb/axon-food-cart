package com.example.axonexample.query.model;

import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * A projected view of a food cart.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FoodCart {
    /**
     * Food cart ID.
     */
    @Id
    private UUID foodCartId;

    /**
     * A list of products and their quantities.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    private Map<UUID, Integer> products = new HashMap<>();

    /**
     * Whether the food cart is confirmed.
     */
    private boolean confirmed = false;

    /**
     * Adds the given product to the food cart with the given quantity.
     * <p>
     * If the product is already in the cart, the provided quantity will be added to the existing quantity.
     *
     * @param productId ID of the product.
     * @param quantity  Quantity of the product to add.
     */
    public void addProduct(UUID productId, int quantity) {
        products.compute(productId, (k, v) -> {
            if (v == null) {
                return quantity;
            } else {
                return v + quantity;
            }
        });
    }

    /**
     * Removes the given product from the food cart with the given quantity.
     * <p>
     * If the resulting quantity of the product less than or equal to zero, it is removed from the cart entirely.
     *
     * @param productId ID of the product.
     * @param quantity  Quantity of the product to remove.
     */
    public void removeProduct(UUID productId, int quantity) {
        int result = products.compute(productId, (k, v) -> {
            if (v == null) {
                return 0;
            } else {
                return v - quantity;
            }
        });

        if (result <= 0) {
            products.remove(productId);
        }
    }
}
