package com.example.axonexample.query.controller;

import com.example.axonexample.core.command.ConfirmOrderCommand;
import com.example.axonexample.core.command.CreateFoodCartCommand;
import com.example.axonexample.core.query.CollectionWrapper;
import com.example.axonexample.core.query.FindFoodCartQuery;
import com.example.axonexample.core.query.ListFoodCartsQuery;
import com.example.axonexample.query.model.FoodCart;
import lombok.Data;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * A REST controller that acts as an entry point for CQRS commands and queries related to the food cart aggregate.
 */
@Data
@RestController
public class FoodOrderingController {
    /**
     * Axon command gateway bean.
     */
    private final CommandGateway commandGateway;

    /**
     * Axon query gateway bean.
     */
    private final QueryGateway queryGateway;

    /**
     * Creates a new food cart.
     *
     * @return The result of the creation operation.
     */
    @PostMapping("/")
    public CompletableFuture<?> create() {
        return commandGateway.send(new CreateFoodCartCommand());
    }

    /**
     * Returns the food cart with the given ID.
     *
     * @param foodCartId Food cart ID (as a UUID).
     * @return The matching food cart, or not found.
     */
    @GetMapping("/{foodCartId}")
    public CompletableFuture<FoodCart> get(@PathVariable("foodCartId") String foodCartId) {
        return queryGateway.query(new FindFoodCartQuery(UUID.fromString(foodCartId)), ResponseTypes.instanceOf(FoodCart.class));
    }

    /**
     * Returns a list of all available food carts.
     *
     * @return A list of food carts.
     */
    @GetMapping("/")
    public CompletableFuture<List<FoodCart>> list() {
        return queryGateway.query(new ListFoodCartsQuery(), ResponseTypes.instanceOf(CollectionWrapper.class)).thenApply(CollectionWrapper::entries);
    }

    /**
     * Confirms a food cart with the given ID.
     *
     * @param foodCartId Food cart ID (as a UUID).
     * @return The result of the operation.
     */
    @PostMapping("/{foodCartId}:confirm")
    public CompletableFuture<?> confirm(@PathVariable("foodCartId") String foodCartId) {
        return commandGateway.send(new ConfirmOrderCommand(UUID.fromString(foodCartId)));
    }
}
