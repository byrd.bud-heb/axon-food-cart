package com.example.axonexample.query.repository;

import com.example.axonexample.query.model.FoodCart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * A JPA repository for {@link FoodCart} objects.
 */
public interface FoodCartRepository extends JpaRepository<FoodCart, UUID> {

}
