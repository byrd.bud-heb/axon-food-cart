package com.example.axonexample.query;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodCartQueryApplication {
    public static void main(String[] args) {
        SpringApplication.run(FoodCartQueryApplication.class, args);
    }
}
