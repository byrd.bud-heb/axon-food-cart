package com.example.axonexample.query.service;

import com.example.axonexample.core.event.FoodCartCreatedEvent;
import com.example.axonexample.core.event.OrderConfirmedEvent;
import com.example.axonexample.core.event.ProductDeselectedEvent;
import com.example.axonexample.core.event.ProductSelectedEvent;
import com.example.axonexample.core.query.CollectionWrapper;
import com.example.axonexample.core.query.FindFoodCartQuery;
import com.example.axonexample.core.query.ListFoodCartsQuery;
import com.example.axonexample.query.model.FoodCart;
import com.example.axonexample.query.repository.FoodCartRepository;
import lombok.Data;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Service;

import java.util.Collections;

/**
 * A handler service that responds to CQRS events and queries related to the {@link FoodCart} projection.
 */
@Data
@Service
public class FoodCartService {
    /**
     * Food cart repository bean.
     */
    private final FoodCartRepository foodCartRepository;

    /**
     * Event handler for the {@link FoodCartCreatedEvent} event.
     * <p>
     * The event will result in the creation of a new {@link FoodCart} object in the database.
     *
     * @param event Event to handle.
     */
    @EventHandler
    public void on(FoodCartCreatedEvent event) {
        foodCartRepository.save(new FoodCart(event.foodCartId(), Collections.emptyMap(), false));
    }

    /**
     * Event handler for the {@link ProductSelectedEvent} event.
     * <p>
     * The event will result in a product (and its quantity) being added to the matching {@link FoodCart} object.
     *
     * @param event Event to handle.
     */
    @EventHandler
    public void on(ProductSelectedEvent event) {
        foodCartRepository.findById(event.foodCartId()).ifPresent(view -> view.addProduct(event.productId(), event.quantity()));
    }

    /**
     * Event handler for the {@link ProductDeselectedEvent} event.
     * <p>
     * The event will result in a product (and its quantity) being removed from the matching {@link FoodCart} object.
     *
     * @param event Event to handle.
     */
    @EventHandler
    public void on(ProductDeselectedEvent event) {
        foodCartRepository.findById(event.foodCartId()).ifPresent(view -> view.removeProduct(event.productId(), event.quantity()));
    }

    /**
     * Event handler for the {@link OrderConfirmedEvent} event.
     * <p>
     * The event will result in the {@code confirmed} flag set to {@code true} in the matching
     * {@link FoodCart} object.
     *
     * @param event Event to handle.
     */
    @EventHandler
    public void on(OrderConfirmedEvent event) {
        foodCartRepository.findById(event.foodCartId()).ifPresent(view -> view.setConfirmed(true));
    }

    /**
     * Handler for the {@link FindFoodCartQuery} query.
     * <p>
     * If a matching {@link FoodCart} object is not found, {@code null} is returned.
     *
     * @param query Query object to handle.
     * @return The matching {@link FoodCart} object, or {@code null}.
     */
    @QueryHandler
    public FoodCart handle(FindFoodCartQuery query) {
        return foodCartRepository.findById(query.foodCartId()).orElse(null);
    }

    /**
     * Handler for the {@link ListFoodCartsQuery} query, which responds with all available {@link FoodCart} objects.
     *
     * @param query Query object to handle.
     * @return A list of all available {@link FoodCart} objects.
     */
    @QueryHandler
    public CollectionWrapper handle(ListFoodCartsQuery query) {
        return new CollectionWrapper(foodCartRepository.findAll());
    }
}
