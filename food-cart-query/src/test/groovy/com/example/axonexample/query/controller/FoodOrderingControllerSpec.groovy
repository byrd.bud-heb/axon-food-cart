package com.example.axonexample.query.controller

import com.example.axonexample.core.command.ConfirmOrderCommand
import com.example.axonexample.core.command.CreateFoodCartCommand
import com.example.axonexample.core.query.CollectionWrapper
import com.example.axonexample.core.query.FindFoodCartQuery
import com.example.axonexample.core.query.ListFoodCartsQuery
import com.example.axonexample.query.model.FoodCart
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.messaging.responsetypes.ResponseTypes
import org.axonframework.queryhandling.QueryGateway
import spock.lang.Specification

import java.util.concurrent.CompletableFuture

class FoodOrderingControllerSpec extends Specification {
    CommandGateway commandGateway
    QueryGateway queryGateway
    FoodOrderingController foodOrderingController

    def setup() {
        commandGateway = Mock()
        queryGateway = Mock()
        foodOrderingController = new FoodOrderingController(commandGateway, queryGateway)
    }

    def 'When a new food cart is posted is requested, a CreateFoodCartCommand is submitted'() {
        setup:
        def future = CompletableFuture.completedFuture("foo")
        commandGateway.send({ it instanceof CreateFoodCartCommand }) >> future

        when:
        def result = foodOrderingController.create()

        then:
        result.is future
    }

    def 'When a food cart is requested by its ID, it is queried and returned'() {
        setup:
        def foodCartId = UUID.randomUUID()
        def future = CompletableFuture.completedFuture(Mock(FoodCart))
        queryGateway.query(new FindFoodCartQuery(foodCartId), ResponseTypes.instanceOf(FoodCart)) >> future

        when:
        def result = foodOrderingController.get(foodCartId.toString())

        then:
        result.is future
    }

    def 'When a list of food carts is requested, all food carts are returned'() {
        setup:
        def list = Mock(List<FoodCart>)
        def future = CompletableFuture.completedFuture(new CollectionWrapper(list))
        queryGateway.query(new ListFoodCartsQuery(), ResponseTypes.instanceOf(CollectionWrapper)) >> future

        when:
        def result = foodOrderingController.list()

        then:
        result.get().is list
    }

    def 'When a food cart is requested to be confirmed, a ConfirmOrderCommand is submitted'() {
        setup:
        def foodCartId = UUID.randomUUID()
        def future = CompletableFuture.completedFuture(null)
        commandGateway.send(new ConfirmOrderCommand(foodCartId)) >> future

        when:
        def result = foodOrderingController.confirm(foodCartId.toString())

        then:
        result.is future
    }
}
