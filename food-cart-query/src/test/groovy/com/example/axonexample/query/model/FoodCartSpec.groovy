package com.example.axonexample.query.model

import spock.lang.Specification

class FoodCartSpec extends Specification {
    def 'When a product is added to a FoodCart for the first time, the FoodCart contains it with the correct quantity'() {
        setup:
        def foodCart = new FoodCart()
        def productId = UUID.randomUUID()

        when:
        foodCart.addProduct(productId, 5)

        then:
        foodCart.products[productId] == 5
    }

    def 'When a product is added to a FoodCart for the second time, the quantity is updated'() {
        setup:
        def foodCart = new FoodCart()
        def productId = UUID.randomUUID()
        foodCart.products[productId] = 5

        when:
        foodCart.addProduct(productId, 5)

        then:
        foodCart.products[productId] == 10
    }

    def 'When a product is removed from a FoodCart that contained the product, the quantity is updated'() {
        setup:
        def foodCart = new FoodCart()
        def productId = UUID.randomUUID()
        foodCart.products[productId] = 10

        when:
        foodCart.removeProduct(productId, 5)

        then:
        foodCart.products[productId] == 5
    }

    def 'When a product is removed from a FoodCart and its resulting quantity is == 0, the product is removed from the FoodCart'() {
        setup:
        def foodCart = new FoodCart()
        def productId = UUID.randomUUID()
        foodCart.products[productId] = 5

        when:
        foodCart.removeProduct(productId, 5)

        then:
        !foodCart.products.containsKey(productId)
    }

    def 'When a product is removed from a FoodCart and its resulting quantity is < 0, the product is removed from the FoodCart'() {
        setup:
        def foodCart = new FoodCart()
        def productId = UUID.randomUUID()
        foodCart.products[productId] = 1

        when:
        foodCart.removeProduct(productId, 5)

        then:
        !foodCart.products.containsKey(productId)
    }

    def 'When a product is removed from a FoodCart and was not a part of the cart, nothing happens'() {
        setup:
        def foodCart = new FoodCart()
        def productId = UUID.randomUUID()

        when:
        foodCart.removeProduct(productId, 5)

        then:
        !foodCart.products.containsKey(productId)
    }
}
