package com.example.axonexample.query.service

import com.example.axonexample.core.event.FoodCartCreatedEvent
import com.example.axonexample.core.event.OrderConfirmedEvent
import com.example.axonexample.core.event.ProductDeselectedEvent
import com.example.axonexample.core.event.ProductSelectedEvent
import com.example.axonexample.core.query.FindFoodCartQuery
import com.example.axonexample.core.query.ListFoodCartsQuery
import com.example.axonexample.query.model.FoodCart
import com.example.axonexample.query.repository.FoodCartRepository
import spock.lang.Specification

class FoodCartServiceSpec extends Specification {
    FoodCartRepository foodCartRepository
    FoodCartService foodCartService

    def setup() {
        foodCartRepository = Mock()
        foodCartService = new FoodCartService(foodCartRepository)
    }

    def 'When a FoodCartCreatedEvent is received, a new FoodCart entity is saved'() {
        setup:
        def foodCartId = UUID.randomUUID()

        when:
        foodCartService.on(new FoodCartCreatedEvent(foodCartId))

        then:
        1 * foodCartRepository.save({ FoodCart entity ->
            entity.foodCartId == foodCartId
            entity.products == [:]
            !entity.confirmed
        })
    }

    def 'When a ProductSelectedEvent is received, products are added to the matching FoodCart entity'() {
        setup:
        def foodCartId = UUID.randomUUID()
        def productId = UUID.randomUUID()
        def foodCart = Mock(FoodCart)
        foodCartRepository.findById(foodCartId) >> Optional.of(foodCart)

        when:
        foodCartService.on(new ProductSelectedEvent(foodCartId, productId, 5))

        then:
        1 * foodCart.addProduct(productId, 5)
    }

    def 'When a ProductDeselectedEvent is received, products are removed from the matching FoodCart entity'() {
        setup:
        def foodCartId = UUID.randomUUID()
        def productId = UUID.randomUUID()
        def foodCart = Mock(FoodCart)
        foodCartRepository.findById(foodCartId) >> Optional.of(foodCart)

        when:
        foodCartService.on(new ProductDeselectedEvent(foodCartId, productId, 5))

        then:
        1 * foodCart.removeProduct(productId, 5)
    }

    def 'When a OrderConfirmedEvent is received, the FoodCart is marked confirmed'() {
        setup:
        def foodCartId = UUID.randomUUID()
        def foodCart = Mock(FoodCart)
        foodCartRepository.findById(foodCartId) >> Optional.of(foodCart)

        when:
        foodCartService.on(new OrderConfirmedEvent(foodCartId))

        then:
        1 * foodCart.setConfirmed(true)
    }

    def 'When a FindFoodCartQuery is received and the requested FoodCart exists, it is returned'() {
        setup:
        def foodCartId = UUID.randomUUID()
        def foodCart = Mock(FoodCart)
        foodCartRepository.findById(foodCartId) >> Optional.of(foodCart)

        when:
        def result = foodCartService.handle(new FindFoodCartQuery(foodCartId))

        then:
        result.is foodCart
    }

    def 'When a FindFoodCartQuery is received and the requested FoodCart does not exist, null is returned'() {
        setup:
        def foodCartId = UUID.randomUUID()
        foodCartRepository.findById(foodCartId) >> Optional.empty()

        when:
        def result = foodCartService.handle(new FindFoodCartQuery(foodCartId))

        then:
        result == null
    }

    def 'When a ListFoodCartsQuery is received, all FoodCart objects are returned'() {
        setup:
        def foodCarts = []
        foodCartRepository.findAll() >> foodCarts

        when:
        def result = foodCartService.handle(new ListFoodCartsQuery())

        then:
        result.entries().is foodCarts
    }
}
