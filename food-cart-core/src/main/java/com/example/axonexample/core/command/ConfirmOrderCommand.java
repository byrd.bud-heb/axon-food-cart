package com.example.axonexample.core.command;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

/**
 * Command object that requests that food cart be marked as confirmed.
 *
 * @param foodCartId ID of the food cart.
 */
public record ConfirmOrderCommand(@TargetAggregateIdentifier UUID foodCartId) {

}
