package com.example.axonexample.core.event;

import java.util.UUID;

/**
 * Event representing that a product was removed from a food cart.
 *
 * @param foodCartId ID of the food cart.
 * @param productId  ID of the product.
 * @param quantity   Quantity of the product.
 */
public record ProductDeselectedEvent(UUID foodCartId, UUID productId, int quantity) {

}
