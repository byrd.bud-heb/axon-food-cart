package com.example.axonexample.core.command;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

/**
 * Command that requests that a quantity of a product be added to a food cart.
 *
 * @param foodCartId ID of the food cart.
 * @param productId  ID of the product.
 * @param quantity   Quantity of the product.
 */
public record SelectProductCommand(
    @TargetAggregateIdentifier
    UUID foodCartId,
    UUID productId,
    int quantity
) {

}
