package com.example.axonexample.core.query;

import java.util.UUID;

/**
 * Query that asks for a food cart with a given ID.
 *
 * @param foodCartId ID of the food cart.
 */
public record FindFoodCartQuery(UUID foodCartId) {

}
