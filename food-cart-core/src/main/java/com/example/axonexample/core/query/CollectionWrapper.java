package com.example.axonexample.core.query;

import java.util.List;

/**
 * A list wrapper that works around issues in Axon with converting lists of objects in queries. Query handlers may
 * return a list wrapped in this object and it the contained list may be unwrapped on the query client side.
 *
 * @param list List to wrap.
 */
public record CollectionWrapper(List<?> list) {
    /**
     * Unwraps and returns the list wrapped in this object.
     *
     * @param <T> Type of the unwrapped list.
     * @return The unwrapped list.
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> entries() {
        return (List<T>) list;
    }
}
