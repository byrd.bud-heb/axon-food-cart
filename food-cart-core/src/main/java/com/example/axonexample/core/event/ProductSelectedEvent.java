package com.example.axonexample.core.event;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

/**
 * Event representing that a product was added to a food cart.
 *
 * @param foodCartId ID of the food cart.
 * @param productId  ID of the product.
 * @param quantity   Quantity of the product.
 */
public record ProductSelectedEvent(
    @TargetAggregateIdentifier UUID foodCartId,
    UUID productId,
    int quantity
) {

}
