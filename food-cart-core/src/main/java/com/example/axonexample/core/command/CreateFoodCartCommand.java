package com.example.axonexample.core.command;

/**
 * Command that requests a new food cart be created.
 */
public record CreateFoodCartCommand() {

}
