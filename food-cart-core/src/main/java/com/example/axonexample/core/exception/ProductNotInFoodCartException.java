package com.example.axonexample.core.exception;

import lombok.Getter;

import java.util.UUID;

/**
 * An exception representing that a product was attempted to be removed from a food cart that did not already contain
 * the product.
 */
@Getter
public class ProductNotInFoodCartException extends RuntimeException {
    /**
     * Food cart ID.
     */
    private final UUID foodCartId;

    /**
     * Product ID.
     */
    private final UUID productId;

    /**
     * Constructor.
     *
     * @param foodCartId Food cart ID.
     * @param productId  Product ID.
     */
    public ProductNotInFoodCartException(UUID foodCartId, UUID productId) {
        super("Can not remove product " + productId + " from food cart " + foodCartId + " because the product was not already included in the food cart.");
        this.foodCartId = foodCartId;
        this.productId = productId;
    }
}
