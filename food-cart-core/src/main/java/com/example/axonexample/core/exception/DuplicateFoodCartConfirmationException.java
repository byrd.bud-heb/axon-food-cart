package com.example.axonexample.core.exception;

import lombok.Getter;

import java.util.UUID;

/**
 * An exception representing that an already-confirmed food card was attempted to be confirmed.
 */
@Getter
public class DuplicateFoodCartConfirmationException extends RuntimeException {
    /**
     * Food cart ID.
     */
    private final UUID foodCartId;

    /**
     * Constructor.
     *
     * @param foodCartId Food cart ID.
     */
    public DuplicateFoodCartConfirmationException(UUID foodCartId) {
        super("Can not confirm food cart " + foodCartId + " because it has already been confirmed.");
        this.foodCartId = foodCartId;
    }
}
