package com.example.axonexample.core.event;

import java.util.UUID;

/**
 * Event representing that a food cart was confirmed.
 *
 * @param foodCartId ID of the food cart.
 */
public record OrderConfirmedEvent(UUID foodCartId) {

}
