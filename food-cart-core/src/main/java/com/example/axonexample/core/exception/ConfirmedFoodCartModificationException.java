package com.example.axonexample.core.exception;

import lombok.Getter;

import java.util.UUID;

/**
 * An exception representing that an already-confirmed food card was attempted to be modified.
 */
@Getter
public class ConfirmedFoodCartModificationException extends RuntimeException {
    /**
     * Food cart ID.
     */
    private final UUID foodCartId;

    /**
     * Constructor.
     *
     * @param foodCartId Food cart ID.
     */
    public ConfirmedFoodCartModificationException(UUID foodCartId) {
        super("Can not modify food cart " + foodCartId + " because it has already been confirmed.");
        this.foodCartId = foodCartId;
    }
}
