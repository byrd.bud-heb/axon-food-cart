package com.example.axonexample.core.query;

/**
 * Query that asks for a list of available food carts.
 */
public record ListFoodCartsQuery() {

}
